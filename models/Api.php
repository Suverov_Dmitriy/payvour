<?php

class Api
{
    public $params = [];

    public function __construct()
    {
        $params = require_once(__DIR__ . '/../params.php');
        if (isset($params['api'])) {
            $this->params = $params['api'];
        }
    }


    /**
     * Send request function
     * @param $requestType
     * @param $requestParam
     * @param $additionalParams
     */
    private function sendRequest($requestUrl, $requestType = 'POST', $additionalParams = [])
    {

        $ch = curl_init();
        $header = array(
            'API-key: ' . $this->params['key'],
            //'location: 17.905569 16.69921'
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_URL, $this->params['url'] . $requestUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $requestType);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        if ($additionalParams) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $additionalParams);
        }

        $result = curl_exec($ch);
        curl_close($ch);

        //var_Dump(1, $result);die;

        return $result;
    }

    /**
     * Get payvours by location
     * @return mixed
     */
    public function getPayvoursByLocation()
    {
        return $this->sendRequest('/0.1/get_payvours_by_location.php?location=17.905569%2016.69921', 'GET');
    }

    /**
     * Create payvour
     * @return mixed
     */
    public function createPayvour()
    {
        $data = [
            'location' => '',
            'text' => '',
            'price' => ''
        ];
        return $this->sendRequest('/0.1/create_payvour.php', 'POST', $data);
    }

    /**
     * Signup action
     */
    public function signup()
    {
        $data = [
            'username' => '',
            'password' => ''
        ];

        return $this->sendRequest('/signUp.php', 'POST', $data);
    }

    /**
     * Login
     * @return mixed
     */
    public function login()
    {
        $data = [
            'username' => '',
            'password' => ''
        ];

        return $this->sendRequest('/signIn.php', 'GET', $data);
    }

    public function createMessage($message)
    {
        return 'Message: ' . $message . ' created';
    }

    public function deleteMessage($message)
    {
        return 'Message: ' . $message . ' deleted';
    }

    public function getMessages()
    {
        return 'message text';
    }
}