<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
include_once 'models/Api.php';
/**
 * API handling
 */
$api = new Api();
//header('content-type: text/json');
switch ($_POST['action']) {
    case 'getPayvoursByLocation':
        echo $api->getPayvoursByLocation();
        break;
    case 'getMessages':
        $response = $api->getMessages();
        break;
    case 'deleteMessage':
        $response = $api->deleteMessage($_POST['message']);
        break;
    case 'createMessage':
        $response = $api->createMessage($_POST['message']);
        break;
    default:
        echo 'bad request'. $_POST['action'];
}

echo $response;

