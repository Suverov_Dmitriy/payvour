var gulp = require('gulp'),
    imagemin = require('gulp-imagemin'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    minify = require('gulp-minify'),
    browserSync = require('browser-sync'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch');


gulp.task('images', function() {
    gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('images'))
});

gulp.task('sass', function () {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sass(
            {"bundleExec": true,}
        ).on('error', sass.logError))
        .pipe(gulp.dest('src/css/'));
});

gulp.task('mincss', ['sass'], function () {
    gulp.src('src/css/*.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('css/'));
});

gulp.task('scripts', function() {
    return gulp.src('src/js/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest('src/js/concatjs/'));
});

gulp.task('browser-sync', function () {
  var files = [
    'css/*.css',
    'images/*.png',
    'js/*.js'
  ];
  
  browserSync.init(files, {
    server: {
      baseDir: './'
    }
  });
});

gulp.task('minify', ['scripts'], function () {
    gulp.src('src/js/concatjs/main.js')
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('js/'));
});

gulp.task('watch', function() {
    gulp.watch('src/scss/**/*.scss', ['sass']);
    gulp.watch('src/css/*.css', ['mincss']);
    gulp.watch('src/images/*', ['images']);
    gulp.watch('src/js/*.js', ['scripts']);
    gulp.watch('src/js/concatjs/*.js', ['minify']);
});
gulp.task('default', ['watch']);

